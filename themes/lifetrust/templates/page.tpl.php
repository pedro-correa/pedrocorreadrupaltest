<?php

/**
 * @file
 * Lifetrust's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/lifetrust.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see lifetrust_process_page()
 * @see html.tpl.php
 */

function render_faq($node){
	echo '<div class="faq_element">';
	echo '<a class="faq_question">'.$node->title.'</a>';
	echo '<div class="faq_answer">'.$node->field_answer['und'][0]['safe_value'].'</div>';
	echo '</div>';
}

function render_service($node){
	echo '<li>';
	echo '<h2 class="title clearfix">';
	echo '<img src="sites/default/files/'.$node->field_thumb['und'][0]['filename'].'" alt="'.$node->title.'">';
	echo $node->title;
	echo '</h2>';
	echo '<div class="desc">'.$node->field_service_description['und'][0]['summary'].'</div>';
	echo '<div class="btnsection clearfix"><a data-id="'.$node->vid.'" class="btn moresmall service_button">more</a></div>';
	echo '</li>';
}

?>
<div id="container">

<header>

 <?php if ($secondary_menu): ?>
	<div class="supernav">
		<div class="frame clearfix">
     <?php print theme('links__system_secondary_menu', array(
       'links' => $secondary_menu,
       'attributes' => array(
         'id' => 'secondary-menu-links',
         'class' => array('links', 'inline', 'clearfix'),
       ),
       'heading' => array(
         'text' => t('Secondary menu'),
         'level' => 'h2',
         'class' => array('element-invisible'),
       ),
     )); ?>
		</div>
   </div> <!-- /#secondary-menu -->
 <?php endif; ?>

 <?php if ($main_menu): ?>
	<nav class="mainnav">
		<div class="frame clearfix">
			<strong class="logo"><a href="/"><img src="/<?php print path_to_theme(); ?>/images/logo.png" alt="Lifetrust" /></a></strong>
     <?php print theme('links__system_main_menu', array(
       'links' => $main_menu,
       'attributes' => array(
         'id' => 'main-menu-links',
         'class' => array('links', 'clearfix'),
       ),
       'heading' => array(
         'text' => t('Main menu'),
         'level' => 'h2',
         'class' => array('element-invisible'),
       ),
     )); ?>
   	</div>
	</nav> <!-- /#main-menu -->
 <?php endif; ?>

<?php
$block_name = "";
foreach ($page['header'] as $block_key => $key){
	if (strpos($block_key,"block_") !== false) {
		$block_name = explode("_",$block_key);
		break;
	}
}

if (!empty($block_name)){
	$block = module_invoke('block', 'block_view', $block_name[1]);
	print render($block['content']);
	unset($page['header']['block_'.$block_name[1]]);
}
?>

 <?php print render($page['header']); ?>
</header>
<article>
	<div class="shadow">
		<div class="contentholder">
			<div class="shadowcontentholder clearfix">
				<div class="content">
					<?php if ("contact" != drupal_get_path_alias(current_path())) { ?>
					<div class="contactbox">
						<div class="imgsection"><img src="themes/lifetrust/images/contactman.png" alt="" /></div>
						<div class="contacts">
							<div class="title">Call us today!</div>
							<dl>
								<dt>Tel:</dt>
								<dd>212.653.0840</dd>
								<dt>Fax:</dt>
								<dd>212.653.0844</dd>
							</dl>
							<div class="btnsection"><a href="/contact" class="btn contactus">contact us</a></div>
						</div>
					</div>
					<?php } ?>

	<?php
 	  if ("faq" == drupal_get_path_alias(current_path())) {
		  $query = new EntityFieldQuery();
		  $entities = $query->entityCondition('entity_type', 'node')
		  ->propertyCondition('type', 'faq_questions')
		  ->propertyCondition('status', 1)
		  ->execute();

		  if (!empty($entities['node'])) {
				echo '<div class="contactholder">';
				foreach ($entities['node'] as $key=>$node){
					$node_obj = node_load($key);
					render_faq($node_obj);
				}
				echo '</div>';
		  }
		} else if ("services" == drupal_get_path_alias(current_path())) {
			$query = new EntityFieldQuery();
			$entities = $query->entityCondition('entity_type', 'node')
			->propertyCondition('type', 'services')
			->propertyCondition('status', 1)
			->execute();

			if (!empty($entities['node'])) {
				$services_popups = '';
				echo '<ul class="services">';
				foreach ($entities['node'] as $key=>$node){
					$node_obj = node_load($key);
					render_service($node_obj);
					echo print_r();
					$services_popups .= '<div class="service_modal" title="'.$node_obj->title.'" id="service-modal-'.$node_obj->vid.'">'.$node_obj->field_service_description['und'][0]['safe_value'].'</div>';
				}
				echo '</ul>';
				echo $services_popups;
			}
		}
	?>

   <?php print render($page['help']); ?>
   <?php if ($action_links): ?>
     <ul class="action-links">
       <?php print render($action_links); ?>
     </ul>
   <?php endif; ?>
	<?php
		$node_num = "";
		foreach ($page['content']['system_main']['nodes'] as $node_key => $key){
			$node_num = $node_key;
			break;
		}

		if (!empty($node_num)){
			$block = module_invoke('system', 'block_view', 'main');
			if ("contact-success" == drupal_get_path_alias(current_path()) || "403" == drupal_get_path_alias(current_path()) || "404" == drupal_get_path_alias(current_path())) {
				echo '<div class="contactholder">';
					print render($block['content']['nodes'][$node_num]['body']);
				echo '</div>';
			}
			else
				print render($block['content']['nodes'][$node_num]['body']);
			unset($page['content']['system_main']);
		}

		if ("contact" == drupal_get_path_alias(current_path())) {
			$webform_name = "";
			foreach ($page['content'] as $block_key => $key){
				if (strpos($block_key,"webform_") !== false) {
					$webform_name = explode("_",$block_key);
					break;
				}
			}

			if (!empty($webform_name)){
				$webform = module_invoke('webform', 'block_view', $webform_name[1]);
				unset($page['content']['webform_'.$webform_name[1]]);
			}
		}
	?>

	<?php print render($page['content']); ?>

	<?php if ("contact" == drupal_get_path_alias(current_path())) { ?>
	<div class="contactholder">
		<div class="contactform">
				<p>We would to hear from you!  Feel free to contact us at any time regarding <br /> any questions or concerns you may have.  <span class="required">*</span> <span style="color:white">= required</span></p>
				<?php print render($webform['content']); ?>
		</div>
	</div>
	<?php } ?>

   <?php print $feed_icons; ?>

				</div>
				<?php if ($page['sidebar_first']): ?>
					<div class="sidebar">
					<?php
						$block_name = "";
						foreach ($page['sidebar_first'] as $block_key => $key){
							if (strpos($block_key,"block_") !== false) {
								$block_name = explode("_",$block_key);
								break;
							}
						}

						if (!empty($block_name)){
							$block = module_invoke('block', 'block_view', $block_name[1]);
							print render($block['content']);
							unset($page['sidebar_first']['block_'.$block_name[1]]);
						}
					?>
					</div> <!-- /.sidebar -->
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>

<footer>

	<div class="frame">
		<div class="row clearfix">
			<ul>
				<li><a href="/about-us">About</a></li>
				<li><a href="/services">Services</a></li>
				<li><a href="/why-us">Why Us</a></li>
				<li><a href="/faq">FAQ</a></li>
				<li><a href="/contact">Contact</a></li>
			</ul>
			<div class="privacy"><a href="/privacy-policy">Privacy Policy</a></div>
		</div>
		<div class="row clearfix">
			<div class="copy">
 <?php if ($page['footer']): ?>
	<?php
		$block = module_invoke('block', 'block_view', '1');
		print render($block['content']);
		unset($page['footer']['block_1']);
	?>
 <?php endif; ?>
			</div>

			<div class="by"><a href="http://www.bluefountainmedia.com" target="_blank">Website Design</a> by <a href="http://www.bluefountainmedia.com/blog" target="_blank">Blue Fountain Media</a></div>
		</div>
	</div>
</footer>

</div> <!-- /#container -->
