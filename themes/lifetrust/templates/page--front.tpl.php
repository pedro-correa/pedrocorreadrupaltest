<?php

/**
 * @file
 * Lifetrust's theme implementation for the home page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/lifetrust.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see lifetrust_process_page()
 * @see html.tpl.php
 */
?>
<div id="container" class="homepage">

<header>

 <?php if ($secondary_menu): ?>
	<div class="supernav">
		<div class="frame clearfix">
     <?php print theme('links__system_secondary_menu', array(
       'links' => $secondary_menu,
       'attributes' => array(
         'id' => 'secondary-menu-links',
         'class' => array('links', 'inline', 'clearfix'),
       ),
       'heading' => array(
         'text' => t('Secondary menu'),
         'level' => 'h2',
         'class' => array('element-invisible'),
       ),
     )); ?>
		</div>
   </div> <!-- /#secondary-menu -->
 <?php endif; ?>

 <?php if ($main_menu): ?>
	<nav class="mainnav">
		<div class="frame clearfix">
			<strong class="logo"><a href="/"><img src="/<?php print path_to_theme(); ?>/images/logo.png" alt="Lifetrust" /></a></strong>
     <?php print theme('links__system_main_menu', array(
       'links' => $main_menu,
       'attributes' => array(
         'id' => 'main-menu-links',
         'class' => array('links', 'clearfix'),
       ),
       'heading' => array(
         'text' => t('Main menu'),
         'level' => 'h2',
         'class' => array('element-invisible'),
       ),
     )); ?>
   	</div>
	</nav> <!-- /#main-menu -->
 <?php endif; ?>

<?php
$block = module_invoke('block', 'block_view', '4');
print render($block['content']);
unset($page['header']['block_4']);
?>

 <?php print render($page['header']); ?>
</header>
<article>
	<div class="shadow">
		<div class="homecontent">
			<div class="homeboxes clearfix">

   <?php if ($action_links): ?>
     <ul class="action-links">
       <?php print render($action_links); ?>
     </ul>
   <?php endif; ?>
	<?php
	$block = module_invoke('block', 'block_view', '13');
	print render($block['content']);
	unset($page['content']['block_13']);
	?>
	<?php
	$block = module_invoke('block', 'block_view', '14');
	print render($block['content']);
	unset($page['content']['block_14']);
	?>
	<?php
	$block = module_invoke('block', 'block_view', '15');
	print render($block['content']);
	unset($page['content']['block_15']);
	?>

   <?php print render($page['content']); ?>
   <?php print $feed_icons; ?>

 <?php if ($page['sidebar_second']): ?>
   <div id="sidebar-second" class="column sidebar"><div class="section">
     <?php print render($page['sidebar_second']); ?>
   </div></div> <!-- /.section, /#sidebar-second -->
 <?php endif; ?>
			</div>
		</div>
	</div>
</article>

<footer>
 <div class="frame">
		<div class="row clearfix">
			<ul>
				<li><a href="/about-us">About</a></li>
				<li><a href="/services">Services</a></li>
				<li><a href="/why-us">Why Us</a></li>
				<li><a href="/faq">FAQ</a></li>
				<li><a href="/contact">Contact</a></li>
			</ul>
			<div class="privacy"><a href="/privacy-policy">Privacy Policy</a></div>
		</div>
		<div class="row clearfix">
			<div class="copy">
 <?php if ($page['footer']): ?>
	<?php
		$block = module_invoke('block', 'block_view', '1');
		print render($block['content']);
		unset($page['footer']['block_1']);
	?>
 <?php endif; ?>
			</div>

			<div class="by"><a href="http://www.bluefountainmedia.com" target="_blank">Website Design</a> by <a href="http://www.bluefountainmedia.com/blog" target="_blank">Blue Fountain Media</a></div>
		</div>
	</div>
</footer>

</div> <!-- /.homepage -->
